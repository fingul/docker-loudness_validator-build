FROM ubuntu:trusty
RUN apt-get update -y
RUN apt-get install build-essential git scons libboost-all-dev libsndfile-dev -y
WORKDIR /code
RUN git clone https://github.com/mikrosimage/loudness_validator.git /code
VOLUME /out
CMD git pull && scons && cp /code/build/release/app/bin/* /out


# RUN cd /etc/apt && \
#     sed -i 's/archive.ubuntu.com/ftp.neowiz.com/g' sources.list


# ==> Copying Ansible playbook...
#WORKDIR /tmp
#ADD  ../../ansible-role-loudness_validator  /tmp/roles/

# ==> Creating inventory file...
#RUN echo localhost > inventory

# ==> Executing Ansible...
#RUN ansible-galaxy install fingul.loudness_validator
#ADD playbook.yml .
#RUN ansible-playbook -i inventory playbook.yml --connection=local --sudo

#VOLUME /loudness-bin
#CMD cp /usr/local/bin/* /loudness-bin

#RUN ansible-playbook -i inventory ./roles/ansible-role-loudness_validator/tests/playbook.yml --connection=local --sudo

#RUN ansible-galaxy install -r install_roles.yml

#RUN ansible-playbook -i inventory playbook.yml --connection=local --sudo #