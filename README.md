
# loudness_validator-build


# usage - METHOD#1 download prebuilt version

<https://bitbucket.org/fingul/docker-loudness_validator-build/raw/master/bin/loudness-analyser>

<https://bitbucket.org/fingul/docker-loudness_validator-build/raw/master/bin/loudness-corrector>

# usage - METHOD#2 build fresh version

    docker run -v `pwd`/bin:/out fingul/loudness_validator-build

Then, check out `./bin` 


# local build (just for me) 

    docker build --tag=fingul/loudness_validator-build .
    docker run -v `pwd`/bin:/out fingul/loudness_validator-build


# deploy (just for me)

    docker push fingul/loudness_validator-build